export const TOGGLE_MENU = 'TOGGLE_MENU';
export const SET_SITE = 'SET_SITE';
export const SET_SITES = 'SET_SITES';
export const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';