import Heading from './Heading/Heading';
import SubHeading from './SubHeading/SubHeading';
import Title from './Title/Title';
import SubTitle from './SubTitle/SubTitle';
import Paragraph from './Paragraph/Paragraph';
import Emphasis from './Emphasis/Emphasis';
import SearchIcon from './SearchIcon/SearchIcon';
import Dropdown from  './Dropdown/Dropdown';

export {
  Dropdown,
  Emphasis,
  Heading,
  Paragraph,
  SearchIcon,
  SubHeading,
  SubTitle,
  Title,
}