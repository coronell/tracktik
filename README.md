# Siter
Siter is a WebApp that allows you to browser through the company's site and see the site's details.

This app was created using `CRA` for a quick start.

## How to install
This project was developed using `yarn` as the package manager, so in order to install please run
```
yarn install
```
## Usage
Siter provides 3 basic pages and was built keeping in mind that the project may grow, some of the tools added 
  
1. `/` will guide you to `home` which contains a link to the most basic pages
2. `/sites` will display a list of all sites available, no filter was applied
3. `/sites/:id` will display the details of a chosen site 

## What was used
* Create-React-App
* RectRouterDOM
* FontAwesome
* Node-Sass
* Redux
* axios

## What is next?
There are a few things that can be improved
* Transition and animations
* Implement filters and search
* Add more pages to display further information